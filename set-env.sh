#!/usr/bin/printf ERROR: This is a configuration file. Exitting now ..%.s\n
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# File: set-env.sh
# Description: Set environment values for this image
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# User-configurable values
# Already defined ones must take precedence over the ones found on this file.
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

# Image name and version
export OCI_IMAGE_TITLE=${OCI_IMAGE_TITLE:-"novnc"};
export OCI_IMAGE_VERSION=${OCI_IMAGE_VERSION:-"local"};
export OCI_IMAGE_DESCRIPTION=${OCI_IMAGE_DESCRIPTION:-"noVNC in a bubble"};
export OCI_IMAGE_AUTHORS=${OCI_IMAGE_AUTHORS:-"CieNTi <cienti@cienti.com>"};

# Image variant (optional)
#   Example: ubuntu20.04
export OCI_VARIANT_TITLE=${OCI_VARIANT_TITLE:-"ubuntu20.04"};

# Image name and version used as a base for this image
export OCI_FROM_TITLE=${OCI_FROM_TITLE:-"ubuntu"};
export OCI_FROM_VERSION=${OCI_FROM_VERSION:-"20.04"};

# Non-privileged user to create and use inside container
export OCI_USER_UID=${OCI_USER_UID:-"1000"};
export OCI_USER_GID=${OCI_USER_GID:-"1000"};

# Use always a registry. DockerHub one is "docker.io/library"
export OCI_PROD_REGISTRY=${OCI_PROD_REGISTRY:-"docker.io/library"};
export OCI_DEVEL_REGISTRY=${OCI_DEVEL_REGISTRY:-"priv.reg.com/grp/prj"};

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# Computed values
# Already defined ones will be overwritten by the ones found below
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

# Image name and version used as a base for this image
#   Example: ubuntu:20.04
export OCI_FROM_IMAGE="${OCI_FROM_TITLE}:${OCI_FROM_VERSION}";
#   Example: docker.io/library/ubuntu:20.04
export OCI_FROM_IMAGE_FQN="${OCI_PROD_REGISTRY}/${OCI_FROM_IMAGE}"

# Image name and version
#   Example: image:0.0.1
export OCI_IMAGE="${OCI_IMAGE_TITLE}:${OCI_IMAGE_VERSION}";
#   Example: priv.reg.com/grp/prj/image:0.0.1
export OCI_IMAGE_FQN="${OCI_DEVEL_REGISTRY}/${OCI_IMAGE}"

# Image variant
#   Example: 0.0.1-ubuntu20.04
export OCI_VARIANT_IMAGE_VERSION="${OCI_IMAGE_VERSION}-${OCI_VARIANT_TITLE}";
#   Example: image:0.0.1-ubuntu20.04
export OCI_VARIANT_IMAGE="${OCI_IMAGE_TITLE}:${OCI_VARIANT_IMAGE_VERSION}";
#   Example: priv.reg.com/grp/prj/image:0.0.1-ubuntu20.04
export OCI_VARIANT_IMAGE_FQN="${OCI_DEVEL_REGISTRY}/${OCI_VARIANT_IMAGE}";
