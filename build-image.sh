#!/usr/bin/env bash
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# File: build-image.sh
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# Description:
#   Build an image with the expected parameters
set -ex;

# # Configure variant
# export OCI_FROM_TITLE="ubuntu";
# export OCI_FROM_VERSION="20.10";
# export OCI_VARIANT_TITLE="${OCI_FROM_TITLE}${OCI_FROM_VERSION}";

# Seed the environment with defaults
source set-env.sh;

# Go!
docker build \
  --rm \
  --pull \
  --build-arg OCI_FROM_IMAGE_FQN \
  --build-arg OCI_VARIANT_TITLE \
  --build-arg OCI_USER_UID \
  --build-arg OCI_USER_GID \
  --label org.opencontainers.image.title="${OCI_IMAGE_TITLE}" \
  --label org.opencontainers.image.version="${OCI_IMAGE_VERSION}" \
  --label org.opencontainers.image.variant="${OCI_VARIANT_TITLE}" \
  --label org.opencontainers.image.description="${OCI_IMAGE_DESCRIPTION}" \
  --label org.opencontainers.image.authors="${OCI_IMAGE_AUTHORS}" \
  --label maintainer="${OCI_IMAGE_AUTHORS}" \
  -t "${OCI_VARIANT_IMAGE_FQN}" \
.;
