# Base image to be used
ARG OCI_FROM_IMAGE_FQN="docker.io/library/ubuntu:20.04"
FROM ${OCI_FROM_IMAGE_FQN}

# Default variant
ARG OCI_VARIANT_TITLE="ubuntu20.04"

# Default user and group identifiers
ARG OCI_USER_UID="1000"
ARG OCI_USER_GID="1000"

# Install block
RUN set -ex; \
  export DEBIAN_FRONTEND="noninteractive"; \
  # Mandatory update
  apt-get -y update; \
  # Install xvfb, x11vnc and supervisor with no confirmations
  apt-get -y install \
    bash \
    fluxbox \
    git \
    nano \
    net-tools \
    novnc \
    supervisor \
    unzip \
    wget \
    x11vnc \
    xvfb \
    yad \
  ; \
  # Clean stage
  apt-get -y clean; \
  apt-get -y autoclean; \
  apt-get -y autoremove; \
  rm -rf /var/lib/apt/lists/*; \
  # Add default user (no sudo as per official 'best practices' recommendations)
  # If sudo is required, they recommend to try gosu first
  groupadd --gid "${OCI_USER_GID}" "group"; \
  useradd \
    --comment "OCI default user" \
    --home-dir "/home/user" \
    --gid "${OCI_USER_GID}" \
    --no-log-init \
    --no-create-home \
    --no-user-group \
    --shell "/bin/bash" \
    --uid "${OCI_USER_UID}" \
  "user"; \
  # Configure noVNC (original 'vnc.html' with autoconnection at load)
  ln -s "/usr/share/novnc/vnc.html" "/usr/share/novnc/index.html";

ENV \
  # Save image variant globally
  OCI_VARIANT_TITLE="${OCI_VARIANT_TITLE}" \
  # Non-privileged user inside container to be used
  OCI_USER_UID="${OCI_USER_UID}" \
  OCI_USER_GID="${OCI_USER_GID}" \
  # Configure X11 (Xvfb)
  DISPLAY=":99" \
  XVFB_RES="1920x1080x24" \
  # Configure VNC (x11vnc and noVNC)
  X11VNC_PORT="5900" \
  NOVNC_PORT="6080" \
  # Define NOVNC_CMD so a child image can re-use this CMD
  NOVNC_CMD="/home/user/supervisor/run-supervisord.sh"

# Ports to be used
EXPOSE \
  "${X11VNC_PORT}/tcp" \
  "${NOVNC_PORT}/tcp"

# Default to supervisord (child will owns PID)
CMD [ "bash", "-c", "${NOVNC_CMD}" ]

# Configure X, fluxbox, supervisor, ...
COPY --chown=user:group "user-home/" "/home/user/"

# Switch to the user and its home folder
USER user:group
WORKDIR "/home/user"
