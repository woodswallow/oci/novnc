#!/usr/bin/env bash
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# File: run-supervisord.sh
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# Description:
#   Script to be placed somewhere in PATH so when it is used, supervisord is
#   executed, replacing this process by itself one (child owns by exec)
set -ex;

# Ensure log folder exists
if [ ! -d "/home/user/supervisor/log" ];
then
  mkdir -pv "/home/user/supervisor/log";
fi;

# Ensure extra configs folder exists
if [ ! -d "/home/user/supervisor/conf.d" ];
then
  mkdir -pv "/home/user/supervisor/conf.d";
fi;

# Pass control to supervisord
exec supervisord -c "/home/user/supervisor/supervisord.conf";
